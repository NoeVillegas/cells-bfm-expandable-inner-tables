{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-expandable-inner-tables>` Description.

    Example:

    ```html
    <cells-bfm-expandable-inner-tables></cells-bfm-expandable-inner-tables>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-bfm-expandable-inner-tables | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmExpandableInnerTables extends Polymer.Element {

    static get is() {
      return 'cells-bfm-expandable-inner-tables';
    }

    static get properties() {
      return {
        title: String,
        subTitle: String,
        message: String,
        amount: {
          type: Object,
          value: () => ({})
        },
        opened: {
          type: Boolean,
          value: true
        },
        items: {
          type: Array,
          value: () => ([])
        },
        contents: {
          type: Array,
          value: () => ([])
        },
        buttonClear: {
          type: Object,
          value: () => ({})
        },
        buttonApply: {
          type: Object,
          value: () => ({})
        },
        input: {
          type: Array,
          value: () => ([])
        },
        ActiveOption: String,
        summaryData: {
          type: Array,
          value: () => ([])
        },
        summaryChartData: {
          type: Object,
          value: () => ({})
        }
      };
    }
    _handleClickHeader() {
      this.opened = !this.opened;
    }
    _clickedButton(e) {
      let event = e.model.__data.item.event;
      this.set('ActiveOption', event);
    }
    _validDisplay(display, type) {
      return display === type;
    }
  }
  customElements.define(CellsBfmExpandableInnerTables.is, CellsBfmExpandableInnerTables);
}